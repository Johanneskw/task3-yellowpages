﻿using System;
using System.Collections.Generic;

namespace ContactSearch
{
    class Program
    {
        static string[] contacts = { "Albert Einstein", "Blaise Pascal", "Edmond Halley", "Jane Goodall", "Johannes Kepler" };
        static List<Person> persons; 
        static void Main(string[] args)
        {
            Random r = new Random();
            persons = new List<Person>();
            foreach (string name in contacts)
            {
                //creating Person objects for all names in the contacts arrays, generating random numbers and adding to list.
                string[] bits = name.Split(" ");
                Person person = new Person(bits[0], bits[1], r.Next(10000000));
                persons.Add(person);
            }
            //Starting interaction 
            DisplayWelcome();
        }

        public static void DisplayWelcome()
        {
            Console.WriteLine("Hello, please insert search: ");
            string res = Console.ReadLine();
            SearchContacts(res);
        }

        static public void SearchContacts(string res)
        {
            //Splitting input in case of multiple names input. 
            string[] names = res.Split(" ");
            foreach (Person pers in persons)
            {
                string fullname = pers.Firstname + " " + pers.Lastname;
                //Checkin for full match.
                if (res.ToUpper() == fullname.ToUpper()) 
                {
                    Console.WriteLine("Full match: ");
                    PrintContact(pers);
                }
                else
                {
                    //checks for partial matches 
                    foreach (string n in names)
                    {
                        if (fullname.ToUpper().Contains(n.ToUpper()))
                        {
                            Console.WriteLine($"Partial match: ");
                            PrintContact(pers);
                        }
                    }
                }
            }
        }
        //Print function writing out all info about persons.
        public static void PrintContact(Person p)
        {
            string fullname = p.Firstname + " " + p.Lastname; 
            Console.WriteLine("Name: " + p.Firstname + " " + p.Lastname);
            Console.WriteLine("Number: " + p.Number);
            Console.WriteLine("Address: " + p.Address);
            Console.WriteLine();
        }
    }
}
