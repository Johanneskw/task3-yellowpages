﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace ContactSearch
{
    class Person
    {   //Person properties
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int Number { get; set; }
        public string Address { get; set; }
        //overloaded constructor. 
        public Person(string firstname, string lastname, int number, string address)
        {
            Firstname = firstname;
            Lastname = lastname;
            Number = number;
            Address = address;
        }
        public Person(string firstname, string lastname, int number)
        {
            Firstname = firstname;
            Lastname = lastname;
            Number = number;
            Address = "None";
        }

        public Person(string firstname, string lastname)
        {
            Firstname = firstname;
            Lastname = lastname;
            Number = 0;
            Address = "None";
        }

    }
}
